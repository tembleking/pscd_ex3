package main

import (
	"fmt"
	"sync"
	"time"

	"github.com/pSpaces/goSpace"
)

var space gospace.Space

func main() {
	space = gospace.NewSpace("park")
	space.Put("ticket", true, 50)
	go door("A")
	go door("B")
	go ticketExpenser()

	wp := sync.WaitGroup{}
	for i := 0; i < 100; i++ {
		wp.Add(1)
		go func(id int) {
			defer wp.Done()
			process(id)
		}(i)
	}
	wp.Wait()
}

func ticketExpenser() {
	door := true
	for {
		space.Get("buyTicket")
		if door {
			space.Put("yourTicket", "A")
		} else {
			space.Put("yourTicket", "B")
		}
		door = !door
	}
}

func door(doorLetter string) {
	go func() {
		for {
			space.Get("queueDoor", doorLetter)
			var numOfTickets int
			t, _ := space.Get("ticket", true, &numOfTickets)
			numOfTickets = t.GetFieldAt(2).(int)
			containsTickets := numOfTickets-1 > 0
			space.Put("ticket", containsTickets, numOfTickets-1)
			space.Put("doorGivesAccess", doorLetter)
		}
	}()

	go func() {
		for {
			space.Get("exitDoor", doorLetter)
			var containsTickets bool
			var numOfTickets int
			t, _ := space.Get("ticket", &containsTickets, &numOfTickets)
			numOfTickets = t.GetFieldAt(2).(int)
			space.Put("ticket", true, numOfTickets+1)
			space.Put("canExit", doorLetter)
		}
	}()
}

func process(id int) {
	for i := 0; i < 10; i++ {

		// buys a ticket
		space.Put("buyTicket")
		var door string
		t, _ := space.Get("yourTicket", &door)
		door = t.GetFieldAt(1).(string)

		fmt.Println(id, "Encolandome en la puerta:", door)

		// it queues itself
		space.Put("queueDoor", door)

		fmt.Println(id, "He pedido acceso a la puerta", door)
		space.Get("doorGivesAccess", door)

		// enters into the garden
		fmt.Println(id, "En el jardin")
		time.Sleep(5 * time.Second)

		// asks to get out
		fmt.Println(id, "Me voy por la puerta", door)
		space.Put("exitDoor", door)
		space.Get("canExit", door)

		// gets out
	}
}
